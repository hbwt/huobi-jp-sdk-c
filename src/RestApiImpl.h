#ifndef RESTAPIIMPL_H
#define RESTAPIIMPL_H


#include "UrlParamBuilder.h"
#include "RestApi.h"


typedef struct RestApiImpl {
  char* tradingUrl;
  char* marketQueryUrl;
  char* host;
  char* accessKey;
  char* secretKey;
  RestApi* api;
} RestApiImpl;

void restApiImpInit(RestApiImpl* impl, const char* tradingUrl, const char* marketQueryUrl, const char* host);
RestApiImpl* restApiImplCreate(const char* accessKey, const char* secretKey);
RestApi* getRestAccounts(RestApiImpl* impl);

RestApi* createRequestByGetWithSignature(RestApiImpl* impl, const char* address, UrlParamBuilder* builder);


#endif
