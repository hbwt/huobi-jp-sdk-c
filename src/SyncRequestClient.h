#ifndef SYNCREQUESTCLIENT_H
#define SYNCREQUESTCLIENT_H

#include "Huobi/FeeRate.h"
#include "Huobi/Account.h"
#include "RestApiImpl.h"

#define OFFSET_OF(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#define CONTAINS_OF(PTR, TYPE, MEMBER) ({ const typeof( ((TYPE *)0)->MEMBER ) *__mptr = (PTR);  (TYPE *)( (char *)__mptr - OFFSET_OF(TYPE,MEMBER) );})

typedef struct SyncRequestClient {
  char* accessKey;
  char* secretKey;
  RestApiImpl* apiImpl;
} SyncRequestClient;



SyncRequestClient* createSyncRequestClient(const char* accessKey, const char* secretKey);

Account* getSynAccount();




#endif
