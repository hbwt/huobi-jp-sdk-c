#ifndef URLPARAMBUILDER_H
#define URLPARAMBUILDER_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils/treemap.h"


typedef struct UrlParamBuilder {
  int isPost;
  char* address;
  Map* postMap;
  Map* getMap;
  Map* postStringListMap;
} UrlParamBuilder;

void UrlParamBuilderInit(UrlParamBuilder* builder);
void setAddress(UrlParamBuilder* builder, const char* address);
const char* getAddress(UrlParamBuilder* builder);
UrlParamBuilder* putUrlStr(UrlParamBuilder* builder, const char* pre, const char* param);
void UrlParamBuilderFree(UrlParamBuilder* builder);
const char* getPostBody(UrlParamBuilder* builder);


#endif
