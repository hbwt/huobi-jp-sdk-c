#ifndef JSONWRAPPER_H
#define JSONWRAPPER_H
#include "utils/cJSON.h"

typedef struct JsonWrapper {
    cJSON * json;
    const char*(* getString)(const char* name);

    const char*(* getStringOrDefault)(const char* name, const char* def);

    long (*getLong)(const char* name);

    long (*getLongOrDefault)(const char* name, long def);

    int (*getInt)(const char* name);
    
    int (*getIntOrDefault)(const char* name, int def);

    void (*checkSize)(int index);

    void (*checkMandatoryField)(const char* name);
    
    int (*containKey)(const char* name);


} JsonWrapper;

JsonWrapper* JsonWrapperCreate(const char* jsonStr);
void JsonWrapperDelete(JsonWrapper* jw);
/*


static const char* getString(const char* name);

static const char* getStringOrDefault(const char* name, const char* def);

static long getLong(const char* name);

static long getLongOrDefault(const char* name, long def);

static int getInt(const char* name);

static int getIntOrDefault(const char* name, int def);

static int containKey(const char* name);

static void checkSize(int index);

static void checkMandatoryField(const char* name);
*/

        /*
        Decimal getDecimal(const char* name) const;
        
        Decimal getDecimalOrDefault(const char* name, Decimal def) const;

        JsonWrapper getJsonObjectOrArray(const char* name) const;

        // For array
        JsonWrapper getJsonObjectAt(int index) const;

        JsonWrapper getArrayAt(int index) const;

        size_t size() const;

        long getLongAt(int index) const;

        Decimal getDecimalAt(int index) const;

        const char* getStringAt(int index) const;
        const rapidjson::Value& object;
        */






#endif /* JSONWRAPPER_H */

