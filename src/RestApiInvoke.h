#ifndef RESTAPIINVOKE_H
#define RESTAPIINVOKE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <curl/curl.h>
#include "JsonWrapper.h"

typedef struct String {
    char* ptr;
    size_t len;
} String;

static void initString(String* str) {
    str->len = 0;
    str->ptr = malloc(str->len+1);
    str->ptr[0] = '\0';
}

static size_t writeFun(void *input, size_t uSize, size_t uCount, String* str) {
    size_t newLen = str->len + uSize*uCount;
    str->ptr = realloc(str->ptr, newLen + 1);
    memcpy(str->ptr+str->len, input, uSize*uCount);
    str->ptr[newLen] = '\0';
    str->len = newLen;
    printf("Accounts: \n%s\n", (const char*)str->ptr);
    return uSize*uCount;
}

static void* callSync(RestApi* req) {
    printf("------response------\n");
    String str;
    initString(&str);
    CURLcode code = curl_global_init(CURL_GLOBAL_DEFAULT);
    if (code != CURLE_OK) {
        printf("curl_global_init() _Err\n");
        return NULL;
    }
    CURL* pCurl = curl_easy_init();
    if (pCurl == NULL) {
        printf("curl_easy_init() Err\n");
        return NULL;
    }

    //printf("url: [%s]\n",req->url);
    /*
    curl_easy_setopt(pCurl, CURLOPT_SSLKEYTYPE, "PEM");
    curl_easy_setopt(pCurl, CURLOPT_SSL_VERIFYPEER, 1L);
    curl_easy_setopt(pCurl, CURLOPT_SSL_VERIFYHOST, 1L);
    */
    curl_easy_setopt(pCurl, CURLOPT_URL, req->url); // 访问的URL
    //curl_easy_setopt(pCurl, CURLOPT_CAINFO, "/etc/huobi_cert/cert.pem");
    
    if (!strcmp(req->method,"POST")) {
        curl_easy_setopt(pCurl, CURLOPT_POST, 1);
        struct curl_slist *plist = curl_slist_append(NULL, "Content-Type:application/json;charset=UTF-8");
        curl_easy_setopt(pCurl, CURLOPT_HTTPHEADER, plist);
    } else {
        struct curl_slist *plist = curl_slist_append(NULL, "Content-Type:application/x-www-form-urlencoded");
        curl_easy_setopt(pCurl, CURLOPT_HTTPHEADER, plist);
    }
    curl_easy_setopt(pCurl, CURLOPT_TIMEOUT, 20); // 超时(单位S)
    curl_easy_setopt(pCurl, CURLOPT_WRITEFUNCTION, &writeFun); // !数据回调函数
    curl_easy_setopt(pCurl, CURLOPT_WRITEDATA, &str); // !数据回调函数的参，一般为Buffer或文件fd
    if (!strcmp(req->method,"POST")) {
        //TODO: body需要转成utf-8
        curl_easy_setopt(pCurl, CURLOPT_POSTFIELDS, req->postBody);
    }
    curl_easy_perform(pCurl);
    if (code != CURLE_OK) {
        printf("curl_easy_perform() Err\n");
        return NULL;
    }
    
    //JsonWrapper* jw = JsonWrapperCreate(*sBuffer);
    curl_easy_cleanup(pCurl);
    curl_global_cleanup();

    return NULL;

}


#endif
