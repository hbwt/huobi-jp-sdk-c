#include <stdio.h>
#include <stdlib.h>
#include "SyncRequestClient.h"
#include "RestApiInvoke.h"
#include "openssl/hmac.h"

SyncRequestClient* gSynClient = NULL;
SyncRequestClient* createSyncRequestClient(const char* accessKey, const char* secretKey) {
    SyncRequestClient* client = (SyncRequestClient*)malloc(sizeof(SyncRequestClient));
    if (!client) return NULL;

    client->accessKey = malloc(strlen(accessKey) + 1);
    memset(client->accessKey, 0, strlen(accessKey) + 1);
    memcpy(client->accessKey, accessKey, strlen(accessKey));
    client->secretKey = malloc(strlen(secretKey) + 1);
    memset(client->secretKey, 0, strlen(secretKey) + 1);
    memcpy(client->secretKey, secretKey, strlen(secretKey));
    client->apiImpl = restApiImplCreate(accessKey, secretKey);
    if (gSynClient) free(gSynClient);
    gSynClient = client;
    return client;
}

Account* getSynAccount() {

    if (!gSynClient) return NULL;
    RestApi* api = getRestAccounts(gSynClient->apiImpl);
    callSync(api);
    return NULL;
}

 
