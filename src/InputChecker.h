#ifndef INPUTCHECKER_H
#define INPUTCHECKER_H
#include <ctype.h>

int isSpecialChar(const char* str) {
  if (!str || !strlen(str)) return 0;
  for (int i = 0; i < strlen(str); i++) {
      if (ispunct(str[i])) {
        return 1;
      }
  }
  return 0;
}

char* seperateLine(const char* src, int* start, char delimiter) {
  if (!src || !start) return NULL;
  char* each = NULL;
  for (int i = *start; i <= strlen(src); i++) {
    if (delimiter == src[i] && i > *start) {
        each = malloc(i-*start);
        *start = i+1;
        break;
    }
  }
  return each;
}

int checkSymbol(const char* symbol) {
  if (!symbol || !strlen(symbol)) return -1;
  if (isSpecialChar(symbol)) return -2;
  return 0;
}





#endif
