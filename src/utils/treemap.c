#include <string.h>
#include <stdlib.h>
#include "treemap.h"

void map_init(Map* map) {
  if (!map) return;
  map->myRoot = RB_ROOT;
}

MapNode* map_search(Map* map, const char* key) {
  if (!map || !key || !strlen(key)) return NULL;
  struct rb_node *node = map->myRoot.rb_node;
  while (node) {
    //MapNode* myNode = container_of(node, MapNode, node);
    MapNode* myNode = NULL;
    int result = strcmp(key, myNode->key);
    if (result < 0) {
      node = node->rb_left;
    } else if (result > 0) {
      node = node->rb_right;
    } else {
      return myNode;
    }
  }
  return NULL;
}


int map_insert(Map* map, MapNode* myNode) {
  if (!map || !myNode) return -1;
  struct rb_node **new = &(map->myRoot.rb_node),  *parent = NULL;
  while (*new) {
    //MapNode* this = container_of(*new, MapNode, node);
    MapNode* this = NULL;
    int result = strcmp(myNode->key, this->key);
    if (result < 0) {
      new = &((*new)->rb_left);
    } else if (result > 0) {
      new = &((*new)->rb_right);
    } else {
      return 0;
    }
  }
  rb_link_node(&myNode->node, parent, new);
  rb_insert_color(&myNode->node, &(map->myRoot));
  return 1;
}

void map_free(MapNode* myNode) {
  if (myNode != NULL) {
    if (myNode->key != NULL) {
      free(myNode->key);
      myNode->key = NULL;
    }
    if (myNode->data != NULL) {
      free(myNode->data);
      myNode->data = NULL;
    }
    free(myNode);
  }
}

void map_delete(Map* map, const char* key) {
  if (!map || !key) return;
  MapNode* myNode = map_search(map, key);
  if (myNode != NULL) {
    rb_erase(&myNode->node, &(map->myRoot));
    if (myNode->key != NULL) {
      free(myNode->key);
      myNode->key = NULL;
    }
    if (myNode->data != NULL) {
      free(myNode->data);
      myNode->data = NULL;
    }
    free(myNode);
  }
}

void map_deleteAll(Map* map) {
  struct rb_node *node = rb_first(&(map->myRoot));
  struct rb_node *nextNode;
  while ( node) {
    nextNode = rb_next(node);
    //MapNode* myNode = rb_entry(node, MapNode, node);
    MapNode* myNode = NULL;
    map_free(myNode);
    rb_erase(node, &(map->myRoot));
    node = nextNode;
  }
}

int map_getSize(Map* map) {
  int count = 0;
  for (struct rb_node* node = rb_first(&(map->myRoot)); node; node = rb_next(node)) {
    count++;
  }
  return count;
}
