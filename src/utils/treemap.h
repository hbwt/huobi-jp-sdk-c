#ifndef TREEMAP_H
#define TREEMAP_H
#include "rbtree.h"

typedef struct MapNode {
  struct rb_node node;
  char* key;
  void* data;
} MapNode;

typedef struct Map {
  struct rb_root myRoot;
} Map;


void map_init(Map* map);
MapNode* map_search(Map* map, const char* key);
int map_insert(Map* map, MapNode* node);
void map_free(MapNode* node);
int map_getSize(Map* map);
void map_delete(Map* map, const char* key);
void map_deleteAll(Map* map);



#endif
