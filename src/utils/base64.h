#pragma once
#include <ctype.h>

char*  base64_encode(unsigned const char * s, unsigned int len);
char*  base64_decode(char* const s);
int is_base64(unsigned char c);

