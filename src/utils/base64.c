#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "base64.h"

const char* base64_chars =
"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";


int is_base64(unsigned char c) {
	return (isalnum(c) || (c == '+') || (c == '/'));
}


char* base64_encode(unsigned const char* bytes_to_encode, unsigned int in_len) {
	char* ret = (char*)malloc(strlen((const char*)bytes_to_encode)*16);
	memset(ret, 0, strlen((const char*)bytes_to_encode)*16);
	int retPos = 0;
	int i = 0;
	int j = 0;
	unsigned char char_array_3[3];
	unsigned char char_array_4[4];

	while (in_len--) {
		char_array_3[i++] = *(bytes_to_encode++);
		if (i == 3) {
			char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
			char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
			char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
			char_array_4[3] = char_array_3[2] & 0x3f;

			for (i = 0; (i < 4); i++) {
				memcpy(ret+retPos, &base64_chars[char_array_4[i]], 1);
				retPos++;
			}
			i = 0;
		}
	}

	if (i)
	{
		for (j = i; j < 3; j++)
			char_array_3[j] = '\0';

		char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
		char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
		char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
		char_array_4[3] = char_array_3[2] & 0x3f;

		for (j = 0; (j < i + 1); j++) {
			memcpy(ret+retPos, &base64_chars[char_array_4[j]], 1);
			retPos++;
		}
			//ret += base64_chars[char_array_4[j]];

		while ((i++ < 3)) {
			memcpy(ret+retPos, "=", 1);
			retPos++;
		}
	}
	return ret;
}

char* base64_decode(char* const encoded_string) {
	int in_len = strlen(encoded_string);
	int i = 0;
	int j = 0;
	int in_ = 0;
	unsigned char char_array_4[4], char_array_3[3];
	//std::string ret;
	char* ret = (char*)malloc(strlen(encoded_string)*2);
	memset(ret, 0, strlen(encoded_string)*2);
	int retPos = 0;

	while (in_len-- && (encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
		char_array_4[i++] = encoded_string[in_]; in_++;
		if (i == 4) {
			for (i = 0; i < 4; i++) {
				//char_array_4[i] = base64_chars.find(char_array_4[i]);
				char* chPos = strchr(base64_chars,char_array_4[i]);
				if (chPos)  char_array_4[i] = chPos - base64_chars;
			}

			char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
			char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
			char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

			for (i = 0; (i < 3); i++) {
				memcpy(ret+retPos, &char_array_3[i], 1);
				retPos++;
				//ret += char_array_3[i];
			}

			i = 0;
		}
	}

	if (i) {
		for (j = i; j < 4; j++)
			char_array_4[j] = 0;

		for (j = 0; j < 4; j++) {
			//char_array_4[j] = base64_chars.find(char_array_4[j]);
			char* chPos = strchr(base64_chars,char_array_4[j]);
			if (chPos)  char_array_4[j] = chPos - base64_chars;
		}

		char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
		char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
		char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

		for (j = 0; (j < i - 1); j++) {
			memcpy(ret+retPos, &char_array_3[j], 1);
			retPos++;
			//ret += char_array_3[j];
		}
	}

	return ret;
}
