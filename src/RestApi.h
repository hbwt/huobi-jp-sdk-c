
#ifndef RESTAPI_H
#define RESTAPI_H

typedef struct RestApi {
  char* url;
  char* postBody;
  char method[10];
  void* jsonParser;
} RestApi;


#endif
