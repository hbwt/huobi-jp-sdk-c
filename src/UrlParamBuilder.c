#include "UrlParamBuilder.h"


void UrlParamBuilderInit(UrlParamBuilder* builder) {
  if (!builder) return;
  builder->isPost = 0;
  builder->address = NULL;
  builder->postMap = NULL;
  builder->getMap = NULL;
  builder->postStringListMap = NULL;

}

void setAddress(UrlParamBuilder* builder, const char* address) {
  if (builder->address) free(builder->address);
  builder->address = (char*)malloc(strlen(address) + 1);
  memset(builder->address,0, strlen(address)+1);
  memcpy(builder->address, address, strlen(address));
}


const char* getAddress(UrlParamBuilder* builder) {
  if (!builder) return NULL;
  if (builder->address) return (const char*)builder->address;
  if (!builder->getMap || !map_getSize(builder->getMap)) return (const char*)builder->address;
  for (struct rb_node *rbNode = rb_first(&(builder->getMap->myRoot)); rbNode; rbNode = rb_next(rbNode)) {
    MapNode* myNode = rb_entry(rbNode, MapNode, node);
    int addrLen = strlen(myNode->key) + strlen((char*)myNode->data) + 1;
    if (!strlen(builder->address)) {
      free(builder->address);
      builder->address = (char*)malloc(addrLen + 1);
      memset(builder->address, 0, addrLen+1);
      memcpy(builder->address, myNode->key, strlen(myNode->key));
      memcpy(builder->address + strlen(myNode->key), "=", 1);
      memcpy(builder->address + strlen(myNode->key) + 1, myNode->data, strlen(myNode->data));
    } else {
      addrLen += strlen(builder->address);
      addrLen += 3;
      builder->address = (char*)realloc(builder->address, addrLen);
      int addrPos = strlen(builder->address);
      memcpy(builder->address + addrPos, "&", 1);
      addrPos++;
      memcpy(builder->address + addrPos, myNode->key, strlen(myNode->key));
      addrPos += strlen(myNode->key);
      memcpy(builder->address + addrPos, "=", 1);
      addrPos++;
      memcpy(builder->address + addrPos, myNode->data, strlen(myNode->data));
    }
    return builder->address;
  }

  return NULL;
}

UrlParamBuilder* putUrlStr(UrlParamBuilder* builder, const char* pre, const char* param) {
  if (!builder || !pre || !param || !strlen(param)) return builder;
  if (!builder->getMap) builder->getMap = (Map*)malloc(sizeof(Map));
  map_init(builder->getMap);
  MapNode* myNode = (MapNode*)malloc(sizeof(MapNode));
  myNode->key = (char*)malloc(strlen(pre) + 1);
  memset(myNode->key,0, strlen(pre)+1);
  memcpy(myNode->key, pre, strlen(pre));
  myNode->data = (char*)malloc(strlen(param) + 1);
  memset(myNode->data,0, strlen(param)+1);
  memcpy(myNode->data, param, strlen(param));
  map_insert(builder->getMap, myNode);
  return builder;
}

void UrlParamBuilderFree(UrlParamBuilder* builder) {
  free(builder);
}

const char* getPostBody(UrlParamBuilder* builder) {
  return NULL;
}

