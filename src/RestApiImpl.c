#include "RestApiImpl.h"
#include "ApiSignature.h"
#include "InputChecker.h"


void restApiImpInit(RestApiImpl* impl, const char* tradingUrl, const char* marketQueryUrl, const char* host) {
    if (!impl || !tradingUrl || !marketQueryUrl || !host) return;
    int len = strlen("https://api.huobi.so");
    impl->tradingUrl = (char*)malloc(len+1);
    memset(impl->tradingUrl, 0, len+1);
    memcpy(impl->tradingUrl, "https://api.huobi.so", len);
    len = strlen("https://api.huobi.so:443");
    impl->marketQueryUrl = (char*)malloc(len+1);
    memset(impl->marketQueryUrl, 0, len+1);
    memcpy(impl->marketQueryUrl, "https://api.huobi.so:443", len);
    len = strlen("api-cloud.huobi.co.jp");
    impl->host = (char*)malloc(len+1);
    memset(impl->host, 0, len+1);
    memcpy(impl->host, "api-cloud.huobi.co.jp", len);
    impl->accessKey = NULL;
    impl->secretKey = NULL;
    impl->api = NULL;
}

RestApiImpl* restApiImplCreate(const char* accessKey, const char* secretKey) {
  RestApiImpl* apiImpl = malloc(sizeof(RestApiImpl));
  apiImpl->accessKey = malloc(strlen(accessKey) + 1);
  memset(apiImpl->accessKey, 0, strlen(accessKey) + 1);
  memcpy(apiImpl->accessKey, accessKey, strlen(accessKey));
  apiImpl->secretKey = malloc(strlen(secretKey) + 1);
  memset(apiImpl->secretKey, 0, strlen(secretKey) + 1);
  memcpy(apiImpl->secretKey, secretKey, strlen(secretKey));
  int len = strlen("https://api-cloud.huobi.co.jp");
  apiImpl->tradingUrl = (char*)malloc(len+1);
  memset(apiImpl->tradingUrl, 0, len+1);
  memcpy(apiImpl->tradingUrl, "https://api-cloud.huobi.co.jp", len);
  len = strlen("api-cloud.huobi.co.jp");
  apiImpl->host = (char*)malloc(len+1);
  memset(apiImpl->host, 0, len+1);
  memcpy(apiImpl->host, "api-cloud.huobi.co.jp", len);
 
  return apiImpl;
}

RestApi* getRestAccounts(RestApiImpl* impl) {
    UrlParamBuilder* builder = (UrlParamBuilder*)malloc(sizeof(UrlParamBuilder));
    UrlParamBuilderInit(builder);

    RestApi* res = createRequestByGetWithSignature(impl, "/v1/account/accounts", builder);
    UrlParamBuilderFree(builder);
    return res;
}

RestApi* createRequestByGetWithSignature(RestApiImpl* impl, const char* address, UrlParamBuilder* builder) {
  impl->api = (RestApi*)malloc(sizeof(RestApi));
  memset(impl->api->method, 0, 10);
  memcpy(impl->api->method, "GET", 3);

  const char* builderAddress = getAddress(builder);
  
  char* tail = buildSignaturePath(impl->host, impl->accessKey,impl->secretKey, address, impl->api->method,builderAddress);
    
  int newLen = strlen(address) + 1;
  if (!builderAddress) {
    newLen += (0 + strlen(tail) + 1);
  } else {
    newLen += (strlen(builderAddress) + strlen(tail) + 1);
  }

  char* newAddr = (char*)malloc(newLen);
  memset(newAddr, 0, newLen);
  int newPos = 0;
  if (!builderAddress || !strlen(builderAddress)) {
    memcpy(newAddr, address, strlen(address));
    newPos += strlen(address);
    memcpy(newAddr+newPos, "?", 1);
    newPos++;
    memcpy(newAddr+newPos, tail, strlen(tail));
  } else {
    memcpy(newAddr, address, strlen(address));
    newPos += strlen(address);
    memcpy(newAddr+newPos, "?", 1);
    newPos++;
    memcpy(newAddr+newPos, builderAddress, strlen(builderAddress));
    newPos += strlen(builderAddress);
    memcpy(newAddr+newPos, "&", 1);
    newPos++;
    memcpy(newAddr+newPos, tail, strlen(tail));
  }
  setAddress(builder,newAddr);
  free(newAddr);
  free(tail);
  builderAddress = getAddress(builder);
  impl->api->url = (char*)malloc(strlen(impl->tradingUrl) + strlen(builderAddress) + 1);
  memset(impl->api->url, 0, strlen(impl->tradingUrl) + strlen(builderAddress) + 1);
  memcpy(impl->api->url, impl->tradingUrl, strlen(impl->tradingUrl));
  memcpy(impl->api->url + strlen(impl->tradingUrl), builderAddress, strlen(builderAddress));

  const char* postBody = getPostBody(builder);
  if (!postBody) {
    impl->api->postBody = NULL;
    return impl->api;
  }
  impl->api->postBody = (char*)malloc(strlen(postBody) + 1);
  memset(impl->api->postBody, 0, strlen(postBody) + 1);
  memcpy(impl->api->postBody, postBody, strlen(postBody));
  
  return impl->api;

}

void getApiFeeRate(const char* symbols) {
  if (!symbols || !strlen(symbols)) return;
  int start = 0;
  char* each = NULL;
  while((each = seperateLine(symbols, &start, ','))) {
    int result = checkSymbol((const char*)each);
    free(each);
    if (result < 0) {
      return;
    }
  }
  UrlParamBuilder builder;
  putUrlStr(&builder, "symbols", (const char*)escapeURL(symbols) );

  return;

}


