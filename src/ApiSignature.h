#ifndef APISIGNATURE_H
#define APISIGNATURE_H
#include <time.h>
#include <stdio.h>
#include "utils/base64.h"
#include "openssl/hmac.h"

char dec2hexChar(short int n) {
    if (0 <= n && n <= 9) {
        return (char)((short)('0') + n);
    } else if (10 <= n && n <= 15) {
        return (char)((short)('A') + n - 10);
    } else {
        return (char)(0);
    }
}

char* escapeURL(const char* URL) {
  if (!URL) return NULL;
  int len = strlen(URL);
  if (!len) return NULL;
  char* result = malloc(len*3);
  memset(result, 0, len*3);
  int resultPos = 0;
  for (int i = 0; i < strlen(URL); i++) {
    char c = URL[i];
    if (
       ('0' <= c && c <= '9') ||
       ('a' <= c && c <= 'z') ||
       ('A' <= c && c <= 'Z') ||
       ('/' == c || '.' == c)) {
         memcpy(result+resultPos, &URL[i], 1);
         resultPos++;
     } else {
       int j = (short int)c;
       if (j < 0) {
         j += 256;
       }
       int i1, i0;
       i1 = j / 16;
       i0 = j - i1*16;
       memcpy(result+resultPos, "%", 1);
       resultPos++;
       char hChar = dec2hexChar(i1);
       memcpy(result+resultPos, &hChar, 1);
       resultPos++;
       hChar = dec2hexChar(i0);
       memcpy(result+resultPos, &hChar, 1);
       resultPos++;
     }
  }
  return result;
}

char* CreateSignature(const char* host, const char* accessKey, const char* secretKey, const char* address,
  const char* method, char* timeBuf, const char* param) {
    if (!accessKey || !secretKey || !strlen(accessKey) || !strlen(secretKey)) return NULL;
    char cre[512] = {0};
    int pos = 0;
    memcpy(cre+pos, method, strlen(method));
    pos += strlen(method);
    memcpy(cre+pos, "\n", 1);
    pos++;
    memcpy(cre+pos, host, strlen(host));
    pos += strlen(host);
    memcpy(cre+pos, "\n", 1);
    pos++;
    memcpy(cre+pos, address, strlen(address));
    pos += strlen(address);
    memcpy(cre+pos, "\n", 1);
    pos++;
    memcpy(cre+pos, "AccessKeyId=", strlen("AccessKeyId="));
    pos+= strlen("AccessKeyId=");
    memcpy(cre+pos, accessKey, strlen(accessKey));
    pos += strlen(accessKey);
    memcpy(cre+pos, "&SignatureMethod=HmacSHA256", strlen("&SignatureMethod=HmacSHA256"));
    pos+= strlen("&SignatureMethod=HmacSHA256");
    memcpy(cre+pos, "&SignatureVersion=2&Timestamp=", strlen("&SignatureVersion=2&Timestamp="));
    pos+= strlen("&SignatureVersion=2&Timestamp=");
    memcpy(cre+pos, timeBuf, strlen(timeBuf));
    pos+= strlen(timeBuf);
    if (param && strcmp(param, "")) {
      memcpy(cre + pos, "&", 1);
      pos++;
      memcpy(cre + pos, param, strlen(param));
    }
    unsigned char output[1024] = {0};
    
      const EVP_MD *engine = EVP_sha256();
  
      uint32_t len = 1024;
//openssl 1.0.x
#ifdef OPENSSL_VERSION_1_0
            HMAC_CTX ctx;
            HMAC_CTX_init(&ctx);
            HMAC_Init_ex(&ctx, secretKey, strlen(secretKey), engine, NULL);
            HMAC_Update(&ctx, (unsigned char *) cre, strlen(cre));
            HMAC_Final(&ctx, output, &len);
            HMAC_CTX_cleanup(&ctx);
#endif

//openssl 1.1.x
#ifdef OPENSSL_VERSION_1_1
            HMAC_CTX* ctx = HMAC_CTX_new();
            HMAC_Init_ex(ctx, secretKey, strlen(secretKey), engine, NULL);
            HMAC_Update(ctx, (unsigned char*) cre, strlen(cre));
            HMAC_Final(ctx, output, &len);
            HMAC_CTX_free(ctx);
#endif
    /*
    printf("\n");
    for (int j = 0; j < len; j++) {
      printf("%02X",output[j]);
    }
    printf("\n");
    */
    char* code = base64_encode(output, 32);
    return code;

  }

char* buildSignaturePath(const char* host, const char* accessKey, const char* secretKey,
    const char* address, const char* method, const char* param) {
      time_t t = time(NULL);
      struct tm *local = gmtime(&t);
      char timeBuf[100] = {0};
      sprintf(timeBuf, "%04d-%02d-%02dT%02d%%3A%02d%%3A%02d",
                    local->tm_year + 1900,
                    local->tm_mon + 1,
                    local->tm_mday,
                    local->tm_hour,
                    local->tm_min,
                    local->tm_sec);
      char* signature = CreateSignature(host, accessKey, secretKey, address, method, timeBuf, param);
      
      char* code = escapeURL((const char*)signature);
      char* res = (char*)malloc(1024);
      memset(res, 0, 1024);
      memcpy(res, "AccessKeyId=", strlen("AccessKeyId="));
      int pos = strlen("AccessKeyId=");
      memcpy(res+pos, accessKey, strlen(accessKey));
      pos = pos + strlen(accessKey);
      memcpy(res + pos, "&SignatureMethod=HmacSHA256", strlen("&SignatureMethod=HmacSHA256"));
      pos = pos + strlen("&SignatureMethod=HmacSHA256");
      memcpy(res + pos, "&SignatureVersion=2&Timestamp=", strlen("&SignatureVersion=2&Timestamp="));
      pos = pos + strlen("&SignatureVersion=2&Timestamp=");
      memcpy(res + pos, timeBuf, strlen(timeBuf));
      pos = pos + strlen(timeBuf);
      memcpy(res + pos, "&Signature=", strlen("&Signature="));
      pos = pos + strlen("&Signature=");
      memcpy(res + pos, code, strlen(code));
      free(code);
      free(signature);

      return res;
    }

#endif
