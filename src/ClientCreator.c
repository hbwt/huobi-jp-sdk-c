#include <stdio.h>
#include "Huobi/RequestClient.h"
#include "SyncRequestClient.h"


RequestClient* createRequestClient(const char* accessKey, const char* secretKey) {
    RequestClient* client = (RequestClient*)malloc(sizeof(RequestClient));
    createSyncRequestClient(accessKey, secretKey);
    client->getAccount = getSynAccount;
    return client;
}