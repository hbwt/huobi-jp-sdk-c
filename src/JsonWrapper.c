#include <stdlib.h>
#include "JsonWrapper.h"



void JsonWrapperDelete(JsonWrapper* jw) {
    cJSON_Delete(jw->json);
    free(jw);
}


static const char* getString(const char* name) {
    return NULL;
}

static const char* getStringOrDefault(const char* name, const char* def) {
    return NULL;
}

static long getLong(const char* name) {
    return 0;
}

static long getLongOrDefault(const char* name, long def) {
    return 0;
}

static int getInt(const char* name) {
    return 0;
}

static int getIntOrDefault(const char* name, int def) {
    return 0;
}

static int containKey(const char* name) {
    return 0;
}

static void checkSize(int index) {
    return;
}

static void checkMandatoryField(const char* name) {
    return;
}

JsonWrapper* JsonWrapperCreate(const char* jsonStr) {
    JsonWrapper* jw = (JsonWrapper*)malloc(sizeof(JsonWrapper));
    jw->getString = getString;
    jw->getStringOrDefault = getStringOrDefault;
    jw->getLong = getLong;
    jw->getLongOrDefault = getLongOrDefault;
    jw->getInt = getInt;
    jw->getIntOrDefault = getIntOrDefault;
    jw->containKey = containKey;
    jw->checkMandatoryField = checkMandatoryField;
    jw->checkSize = checkSize;

    jw->json = cJSON_Parse(jsonStr);
    if (!jw->json) {
        free(jw);
        jw = NULL;
    } 
    return jw;
}



