# Huobi C SDK Change Log



This is Huobi C SDK, This is a lightweight c library, you can import to your c project and use this SDK to query your account now and all market data, trading data and managing  your account in future.



The SDK supports synchronous RESTful API invoking and  is going to support subscribing  the market data from the Websocket connection.



## Table of Contents

- [Huobi Global API C SDK version 0.0.1](#Huobi-Global-API-c-SDK-version-0.0.1)


## Huobi Global API C SDK version 0.0.1

[***version 0.0.1***](https://userid@bitbucket.org/hbwt/huobi-jp-sdk-c.git)

 ***2019-11-18***  

1. **Supported following REST endpoints:**

```
 GET /v1/account/accounts
```
