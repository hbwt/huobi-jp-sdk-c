# Huobi C SDK (version 0.0.1)

This is Huobi C SDK, This is a lightweight c library, you can import to your C project and use this SDK to query your account now and all market data, trading data and managing your account in future.

The SDK supports synchronous RESTful API invoking, and is going to support subscribing  the market data from the Websocket connection.


## Huobi C SDK Download
[Huobi Global API C SDK version 0.0.1](https://userid@bitbucket.org/hbwt/huobi-jp-sdk-c.git)

### Installation

*The SDK is compiled by gnuC99*.

You can compile the SDK with gcc.

Currently, The SDK has the compatibility on linux system(centos 7 and ubuntu 18.04) only.

Later, macOS and windows.

#### Install CMake

Please make sure the CMake is installed on your OS.

If not, you can follow <https://cmake.org/install/> to install it.

The minimum required version of CMake is 2.8, but we suggest to use the lastest CMake version.

#### Install 3rd party

Please make sure the 3rd party libraries have been installed in your system. If not, please install them.

OpenSSL - <https://github.com/openssl/openssl>

curl - <https://github.com/curl/curl>


#### dependency libraries installation

ubuntu 18.04:
````
#cmake
$ sudo apt install cmake
#openssl
$ sudo apt install openssl
$ sudo apt install libssl-dev
#curl
$ sudo apt install curl libcurl4-openssl-dev
#gcc
$sudo apt update
$sudo apt install build-essential
````

centos 7   

#cmake
$ sudo yum install cmake
#openssl
$ sudo yum install openssl openssl-devel
#curl
$ sudo yum install libcurl libcurl-devel
#gcc
$ sudo yum install gcc
$ sudo yum install gcc-c++



#### Build SDK

Then build the SDK library

```
$ git clone https://userid@bitbucket.org/hbwt/huobi-jp-sdk-c.git
$ cd huobi-jp-sdk-c
$ mkdir build
$ cd build
#使用gcc
$ cmake  -G "Unix Makefiles" ../
$ make
```

#### Run example

```
<In huobi-jp-sdk-c folder>
$ cd examples
$ cd GetAccount
$ open "main.c" and replace "XXX" in the "createRequestClient("xxx", "xxx")" with your real accessKey and secretKey
$ mkdir build
$ cd build
#use gcc
$ cmake  -G "Unix Makefiles" ../
$ make
#run the program
$./GetAccount


```
