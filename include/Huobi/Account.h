#ifndef ACCOUNT_H
#define ACCOUNT_H



    /**
     * The account information for spot account, margin account etc.
     */
    typedef struct Account {
        /**
         * The unique account id.
         */
        long id;

        /*
         * The type of this account, possible value: spot, margin, otc, point.
         */
        char*  type;

        /*
         * The account state, possible value: working, lock.
         */
        char* state;

        int user_id;


    } Account;


#endif /* ACCOUNT_H */



