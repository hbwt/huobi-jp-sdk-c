#ifndef FEERATE_H
#define FEERATE_H


/**
 * The current fee of taker and maker.
 */
typedef struct FeeRate {
    /**
     * The symbol like "btcusdt".
     */
    char* symbol;

    /**
     * The fee of maker.
     */
    char* maker_fee;
    /**
     * The fee of taker.
     */
    char* taker_fee;
} FeeRate;



#endif /* FEERATE_H */