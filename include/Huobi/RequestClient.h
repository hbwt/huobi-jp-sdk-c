#ifndef REQUESTCLIENT_H
#define REQUESTCLIENT_H
#include "Huobi/FeeRate.h"
#include "Huobi/Account.h"

typedef struct RequestClient {
  Account* (*getAccount)();

  //void (*freeAll)();
} RequestClient;

RequestClient* createRequestClient(const char* accessKey, const char* secretKey);



#endif
